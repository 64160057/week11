package com.watcharaphon.week11;

public class Submarine extends Vehicle implements Swimable{
    public Submarine(String name, String engineName) {
        super(name, engineName);
    }

    @Override
    public String toString() {
        return "Plane(" + this.getName() + ") " + "engine: " + this.getEngineName();
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
    }

    
    
}
