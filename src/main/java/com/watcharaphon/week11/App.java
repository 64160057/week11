package com.watcharaphon.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();

        Plane plane1 = new Plane("Boeing370", "Boeing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Flyable[] flyablesObjects = {bat1, plane1};
        for(int i=0; i<flyablesObjects.length; i++) {
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }

        Snake snake1 = new Snake("Luffy");
        snake1.crawl();

        Crocodile crocodile1 = new Crocodile("seaman");
        crocodile1.swim();

        Crawlable[] crawlablesObjects = {snake1, crocodile1};
        for(int i=0; i<crawlablesObjects.length; i++) {
            crawlablesObjects[i].crawl();
        }

        Human human1 = new Human("Tanang");
        human1.eat();
        human1.sleep();

        Rat rat1 = new Rat("noo");
        rat1.eat();
        rat1.sleep();

        Submarine submarine1 = new Submarine("Too", "99999-7");

        Swimable[] swimablesObjects = {fish1, submarine1, crocodile1};
        for(int i=0; i<swimablesObjects.length; i++) {
            swimablesObjects[i].swim();

        }

        Walkable[] walkablesObjects = {human1, rat1};
        for(int i=0; i<=walkablesObjects.length; i++) {
            walkablesObjects[i].walk();
        }
    }
}
